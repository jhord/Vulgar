## Name

Vulgar - Display of VGA.

## Synopsis

```javascript
_.cc.carbon.Vulgar
.extended(true)
.rom('rom/vga-unicode.rom')
.color('sky', 0x52, 0x52, 0xff)
.boot(document.getElementById('vulgar'), function() {

  this
    .xy(10, 8)
    .write("Hello, world.")
    .xy(8, 8)
    .fg(this.rgb.yellow)
    .put("\u2609")
    .blink(false)
    .show(false)
  ;

}, 'white', 'sky');
```

## Description

Vulgar is a JavaScript library that paints glyphs to an HTML canvas in a
fashion similar to old school VGA displays.  It allows you to embed a
character-mode display inside a document.

## Gnosis

The basic architecture of Vulgar requires that a display be booted with
a ROM image in order to set the proper aspect ratio of the canvas
as well as load the glyph set that will be used for painting.  Glyphs
are bitmap images that are indexed via Unicode code points.  Each
glyph may be painted to the screen at a particular (x, y) coordinate
with unset bits representing a background color and set bits representing
a foreground color.  A cursor mechanism is also provided to simulate
the original blink tag.

### Singletron Interface

Singletron methods allow you to set default parameters for the Vulgar display
prior to booting.  The following methods are available:

###### `width(width)`

Sets the width of the Vulgar display in glyphs.  The actual resolution of
the canvas is a multiplier of this value combined with the extended flag.
The default is 80.

###### `height(height)`

Sets the height of the Vulgar display in glyphs.  The actual resolution of
the canvas is a multiplier of this value and the per-glyph height as set
when loading a rom image.  The default is 25.

###### `extended(flag)`

Sets the extended flag.  When true Vulgar extends the right side of each
glyph cell by 1 pixel making the overall width per glyph 9 pixels instead
of 8.  For glyphs in the range of U+0000-U+001f, this is displayed as a
blank column.  For glyphs above U+001f, the right-most column of the glyph
is duplicated into the extended column.  The default is false.

###### `rom(uri, height)`

Sets the URI and per-glyph cell height for the glyph ROM set.  ROM files
are bitmaps stored as 8xN indexed images.  Since ROMs can vary in cell
height it must be provided to ensure image data can be extracted properly.
The default height is 16.  The default ROM image loaded is the
'vga-ibm-vga8x16.rom' which provides a rustic IBM VGA display.

###### `rgb()`

Returns the color palette object.  This allows for a complete reconfiguration
of the color palette, including palette name mappings, prior to booting.
Be aware that all Vulgar displays share the same palette space so changes
after a call to boot() will be reflected at run-time in all instances.

###### `color(name, r, g, b)`

Allows you to define or re-define a color in the palette.  The name given
is installed as a key within the RGB space and can be referenced easily
using the `rgb()` instanton method described below.

###### `boot(on_boot, rgbf, rgbb)`

Boots the Vulgar display.  The `on_boot()` function will be called as
a method on the Vulgar display object after successfully initializing
the display.  The `rgbf` and `rgbb` parameters allow you to set default
foreground and background colors for the display.  The defaults of
'gray' and 'black' are used respectively.

### Instanton Interface

###### `clear()`

Clears the Vulgar display by filling the canvas background with the current
background color.  Also resets the nozzle position to (0, 0).

###### `blink(flag)`

Sets the cursor blink flag.  When true the cursor blinks at an adjustable
rate via a timer.  If false, the cursor display is controlled by the show
flag as described below.

###### `show(flag)`

Sets the cursor show flag.  When false, the cursor is not displayed.  When
true the cursor is displayed by overlaying the cursor shape with the image
beneath the current nozzle position.  The cursor shape is controlled by
the rmin and rmin settings described below.

###### `rate(rate)`

Sets the blink rate in milliseconds for the cursor.  Blinking is implemented
by alternating the show flag via a timer that is propagated by this rate.

###### `rmin(rmin)`

Sets the cursor row minimum.  This is relative to a single glyph and defines
the first row at which the cursor should appear within a glyph cell.

###### `rmax(rmax)`

Sets the cursor row maximum.  This is relative to a single glyph and defines
the last row at which the cursor should appear within a glyph cell.

###### `color(name, r, g, b)`

Exactly the same as the singeltron `color()` method.  Allows you to define
a new color in the RGB space.

###### `rgb()`

Returns the RGB color palette.  This is an object that indexes color names
to nozzle the ink palette.  The default VGA palette is loaded with the
following names:


```
  0x00 ( 0) black
  0x01 ( 1) blue
  0x02 ( 2) green
  0x03 ( 3) cyan
  0x04 ( 4) red
  0x05 ( 5) magenta
  0x06 ( 6) brown
  0x07 ( 7) gray
  0x08 ( 8) dark_gray
  0x09 ( 9) bright_blue
  0x0a (10) bright_green
  0x0b (11) bright_cyan
  0x0c (12) bright_red
  0x0d (13) bright_magenta
  0x0e (14) yellow
  0x0f (15) white
```

###### `fg(rgb)`

Sets the foreground color using the RGB space.  The value given can either
be a string representing a value installed by `color()`, an index directly
into the color palette, or a `Uint8ClampedArray` representing a nozzle ink.

###### `bg(rgb)`

Sets the background color using the RGB space.  The value given can either
be a string representing a value installed by `color()`, an index directly
into the color palette, or a `Uint8ClampedArray` representing a nozzle ink.

###### `fb(rgbf, rgbb)`

Sets both the foreground and background colors at once.

###### `x(x)`

Sets the nozzle position to the x value given.

###### `y(y)`

Sets the nozzle position to the y value given.

###### `xy(x, y)`

Sets the nozzle position to the (x, y) value given.

###### `inkf(r, g, b)`

Sets the foreground nozzle ink color directly the RGB value provided.

###### `inkb(r, g, b)`

Sets the background nozzle ink color directly the RGB value provided.

###### `put(glyph)`

Puts a single glyph to the display at the current nozzle position.  The
nozzle position is unaffected.

###### `write(glyphs)`

Writes a series of glyphs to the diplay starting at the current nozzle
position.  The nozzle position is moved to a higher x value for each glyph
and will wrap to higher y values when passing beyond the width of the display.

###### `paint(bits)`

Similar to `put()` but paints an image instead of indexing into the glyph
ROM.  You can use this to inefficiently raster your own fonts.

## Dependosis

This library, as published, requires [Abgrund](https://www.gitlab.com/jhord/Abgrund).
Abgrund filth can be removed with some elbow sweat and grease tears.

## Legalosis

This software is information.
It is subject only to local laws of physics.
