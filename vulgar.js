'use strict';

(function(obj) {
  _._('cc.carbon.Vulgar', obj);

  /**
   *  singletron interface
   */

  // mutable screen defaults
  var _width    = 80;
  var _height   = 25;
  var _extended = false;
  var _rom      = {
    uri:    'rom/vga-ibm-vga8x16.rom',
    height: 16
  };

  // the RGB color maps are shared by all screens
  var _rgb = {
    keys: {
      black:          0x0,
      blue:           0x1,
      green:          0x2,
      cyan:           0x3,
      red:            0x4,
      magenta:        0x5,
      brown:          0x6,
      gray:           0x7,
      dark_gray:      0x8,
      bright_blue:    0x9,
      bright_green:   0xa,
      bright_cyan:    0xb,
      bright_red:     0xc,
      bright_magenta: 0xd,
      yellow:         0xe,
      white:          0xf
    },
    inks: [
      new Uint8ClampedArray([0x00, 0x00, 0x00]),
      new Uint8ClampedArray([0x00, 0x00, 0xaa]),
      new Uint8ClampedArray([0x00, 0xaa, 0x00]),
      new Uint8ClampedArray([0x00, 0xaa, 0xaa]),
      new Uint8ClampedArray([0xaa, 0x00, 0x00]),
      new Uint8ClampedArray([0xaa, 0x00, 0xaa]),
      new Uint8ClampedArray([0xaa, 0x55, 0x00]),
      new Uint8ClampedArray([0xaa, 0xaa, 0xaa]),
      new Uint8ClampedArray([0x55, 0x55, 0x55]),
      new Uint8ClampedArray([0x55, 0x55, 0xff]),
      new Uint8ClampedArray([0x55, 0xff, 0x55]),
      new Uint8ClampedArray([0x55, 0xff, 0xff]),
      new Uint8ClampedArray([0xff, 0x55, 0x55]),
      new Uint8ClampedArray([0xff, 0x55, 0xff]),
      new Uint8ClampedArray([0xff, 0xff, 0x55]),
      new Uint8ClampedArray([0xff, 0xff, 0xff])
    ]
  };

  {
    var hex = [0x00, 0x5f, 0x87, 0xaf, 0xd7, 0xff];
    for(var rc = 0, rl = hex.length; rc < rl; rc++) {
      for(var gc = 0, gl = hex.length; gc < gl; gc++) {
        for(var bc = 0, bl = hex.length; bc < bl; bc++) {
          _rgb.inks.push(new Uint8ClampedArray([hex[rc], hex[gc], hex[bc]]));
        }
      }
    }

    var hex = 0x08;
    while(hex < 0xef) {
      _rgb.inks.push(new Uint8ClampedArray([hex, hex, hex]));
      hex += 0x0a;
    }
  }

  // mutators
  obj.width = function(width) {
    _width = width;
    return obj;
  };

  obj.height = function(height) {
    _height = height;
    return obj;
  };

  obj.extended = function(extended) {
    _extended = extended;
    return obj;
  };

  obj.rom = function(uri, height) {
    _rom = {
      uri:    uri,
      height: typeof(height) === 'undefined' ? 16 : height
    };
    return obj;
  };

  obj.rgb = function() {
    return _rgb;
  }

  obj.color = color;

  /**
   *  common interface
   */
  function color(k, r, g, b) {
    if(    typeof(k) !== 'undefined'
        && typeof(r) !== 'undefined'
        && typeof(g) !== 'undefined'
        && typeof(b) !== 'undefined') {

      var i = _rgb.keys[k];
      if(typeof(i) === 'undefined') {
        i = _rgb.inks.length;
        _rgb.keys[k] = i;
      }

      _rgb.inks[i] = new Uint8ClampedArray([r, g, b]);
    }
    return this;
  }

  /**
   *  strappings of a young screen
   */
  obj.boot = function(canvas, on_boot, rgbf, rgbb) {
    // the thing with the stuff
    var vulgar = {
      ready:    false,
      width:    _width,
      height:   _height,
      extended: _extended,
      rom:      _rom.uri,
      rgb:      _rgb.keys,
      _nozzle: {
        canvas: canvas,
        ctx:    canvas.getContext('2d'),
        show:   true,
        blink:  true,
        xpos:   0,
        ypos:   0,
        bits:   8 + (_extended ? 1 : 0),
        rows:   _rom.height,
        rmin:   _rom.height - 3,
        rmax:   _rom.height - 1,
        rate:   350,
        inks:   _rgb.inks,
        rgbf:   _rgb.inks[_rgb.keys[rgbf]],
        rgbb:   _rgb.inks[_rgb.keys[rgbb]],
        head:   undefined,
        cell:   undefined,
        maps:   undefined
      }
    };

    // the stuff the thing does
    vulgar.clear = clear;
    vulgar.blink = blink;
    vulgar.show  = show;
    vulgar.rate  = rate;
    vulgar.rmin  = rmin;
    vulgar.rmax  = rmax;
    vulgar.fg    = fg;
    vulgar.bg    = bg;
    vulgar.fb    = fb;
    vulgar.x     = x;
    vulgar.y     = y;
    vulgar.xy    = xy;
    vulgar.inkf  = inkf;
    vulgar.inkb  = inkb;
    vulgar.put   = put;
    vulgar.write = write;
    vulgar.paint = paint;
    vulgar.color = color;

    // seems like we need another object.  oh we have one.
    vulgar._nozzle.tick = nozzle_tick;
    vulgar._nozzle.scan = nozzle_scan;
    vulgar._nozzle.blit = nozzle_blit;
    vulgar._nozzle.mapc = nozzle_mapc;
    vulgar._nozzle.draw = nozzle_draw;
    vulgar._nozzle.move = nozzle_move;

    vulgar.fg(typeof(rgbf) === 'undefined' ? 'gray'  : rgbf);
    vulgar.bg(typeof(rgbb) === 'undefined' ? 'black' : rgbb);

    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function() {
      if(this.status === 200) {
        vulgar._nozzle.maps = new Uint8Array(this.response);
        init.call(vulgar, on_boot);
      }
      else {
        // FIXME: what to do on error?  apparently write documentation.
        console.log('Failed to load ROM (server returned ' + this.status+ ')');
      }
    });

    xhr.open('GET', vulgar.rom);
    xhr.responseType = 'arraybuffer';
    xhr.send();

    return vulgar;
  };

  function init(on_init) {
    this._nozzle.canvas.width  = this.width  * this._nozzle.bits;
    this._nozzle.canvas.height = this.height * this._nozzle.rows;
    this._nozzle.tick();

    this.ready = true;
    this.clear();

    if(typeof(on_init) === 'function') {
      on_init.call(this);
    }
    return this;
  }

  /**
   *  instance interface
   */

  function clear() {
    if(this.ready) {
      this._nozzle.xpos = 0;
      this._nozzle.ypos = 0;

      this._nozzle.ctx.fillStyle = 'rgb(' + this._nozzle.rgbb.join(',') + ')';
      this._nozzle.ctx.fillRect(0, 0,
        this._nozzle.canvas.width, this._nozzle.canvas.height);

      this._nozzle.scan()
    }
    return this;
  }

  function blink(blink) {
    if(typeof(blink) !== 'undefined') {
      this._nozzle.blink = blink;
      return this;
    }

    return this._nozzle.blink;
  }

  function show(show) {
    if(typeof(show) !== 'undefined') {
      this._nozzle.show = show;
      this._nozzle.draw();
      return this;
    }

    return this._nozzle.show;
  }

  function rate(rate) {
    if(typeof(rate) !== 'undefined') {
      this._nozzle.rate = rate;
      return this;
    }
    return this._nozzle.rate;
  }

  function rmin(rmin) {
    if(typeof(show) !== 'undefined') {
      this._nozzle.rmin = rmin;
      this._nozzle.draw();
      return this;
    }
    return this._nozzle.rmin;
  }

  function rmax(rmax) {
    if(typeof(show) !== 'undefined') {
      this._nozzle.rmax = rmax;
      this._nozzle.draw();
      return this;
    }
    return this._nozzle.rmax;
  }

  function fg(rgb) {
    if(typeof(rgb) !== 'undefined') {
      var ink;

      if(rgb.constructor === Uint8ClampedArray) {ink = rgb;}
      else {
        ink = this._nozzle.inks
          [typeof(rgb) === 'string' ? this.rgb[rgb] : rgb];
      }

      if(typeof(ink) !== 'undefined') {this._nozzle.rgbf = ink;}
      return this;
    }
    return this._nozzle.rgbf;
  }

  function bg(rgb) {
    if(typeof(rgb) !== 'undefined') {
      var ink;

      if(rgb.constructor === Uint8ClampedArray) {ink = rgb;}
      else {
        ink = this._nozzle.inks
          [typeof(rgb) === 'string' ? this.rgb[rgb] : rgb];
      }

      if(typeof(ink) !== 'undefined') {this._nozzle.rgbb = ink;}
      return this;
    }
    return this._nozzle.rgbb;
  }

  function fb(rgbf, rgbb) {
    this.fg(rgbf)
        .bg(rgbb)
    ;
    return this;
  }

  function inkf(r, g, b) {
    if(   typeof(r) !== 'undefined'
       && typeof(g) !== 'undefined'
       && typeof(b) !== 'undefined') {
      this._nozzle.rgbf = new Uint8ClampedArray([r, g, b]);
    }
    return this;
  }

  function inkb(r, g, b) {
    if(   typeof(r) !== 'undefined'
       && typeof(g) !== 'undefined'
       && typeof(b) !== 'undefined') {
      this._nozzle.rgbb = new Uint8ClampedArray([r, g, b]);
    }
    return this;
  }

  function x(xpos) {
    if(typeof(xpos) !== 'undefined') {
      this._nozzle.move(xpos, this._nozzle.ypos);
      return this;
    }
    return this._nozzle.xpos;
  }

  function y(ypos) {
    if(typeof(ypos) !== 'undefined') {
      this._nozzle.move(this._nozzle.xpos, ypos);
      return this;
    }
    return this._nozzle.ypos;
  }

  function xy(xpos, ypos) {
    this._nozzle.move(xpos, ypos);
    return this;
  }

  function put(glyph) {
    if(   this._nozzle.xpos >= 0 && this._nozzle.xpos < this.width
       && this._nozzle.ypos >= 0 && this._nozzle.ypos < this.height) {
      this._nozzle.mapc(glyph);
      this._nozzle.draw();
    }

    return this;
  }

  function write(glyphs) {
    for(var i = 0; i < glyphs.length; i++) {
      this.put(glyphs[i]);

      var xpos = this._nozzle.xpos + 1;
      var ypos = this._nozzle.ypos;
      if(xpos > this.width) {
        ypos++;
        xpos = 0;

        if(ypos > this.height) {break;}
      }
      this._nozzle.move(xpos, ypos);
    }

    return this;
  }

  function paint(bits) {
    this._nozzle.blit(bits);
    this._nozzle.draw();
    return this;
  }

  /**
   *  nozzle interface
   */

  function nozzle_tick() {
    var vulgar = this;

    if(vulgar.blink) {
      vulgar.show = !vulgar.show;
      vulgar.draw();
    }

    window.setTimeout(function() {vulgar.tick();}, vulgar.rate);
  }

  function nozzle_scan() {
    this.cell = this.ctx.getImageData(
      this.xpos * this.bits, this.ypos * this.rows,
        this.bits, this.rows);
  }

  function nozzle_blit(bits) {
    this.cell = this.ctx.createImageData(this.bits, this.rows);

    var i_ofs = 0;
    for(var row = 0; row < this.rows; row++) {
      var bit = 0x80;
      while(bit) {
        var rgb = bits[row] & bit ? this.rgbf : this.rgbb;

        this.cell.data[i_ofs + 0] = rgb[0];
        this.cell.data[i_ofs + 1] = rgb[1];
        this.cell.data[i_ofs + 2] = rgb[2];
        this.cell.data[i_ofs + 3] = 0xff;
        i_ofs += 4;

        bit >>= 1;
      }

      if(this.bits > 8) {
        this.cell.data[i_ofs + 0] = this.rgbb[0];
        this.cell.data[i_ofs + 1] = this.rgbb[1];
        this.cell.data[i_ofs + 2] = this.rgbb[2];
        this.cell.data[i_ofs + 3] = 0xff;
        i_ofs += 4;
      }
    }
  }

  function nozzle_mapc(c) {
    if(typeof(c) === 'string') {
      c = c.charCodeAt(0);
    }

    var r_ofs = c * this.rows;
    if(r_ofs > this.maps.length) {
      return;
    }

    this.cell = this.ctx.createImageData(this.bits, this.rows);

    var i_ofs = 0;
    for(var row = 0; row < this.rows; row++) {
      var bit = 0x80;
      while(bit) {
        var rgb = this.maps[r_ofs] & bit ? this.rgbf : this.rgbb;

        this.cell.data[i_ofs + 0] = rgb[0];
        this.cell.data[i_ofs + 1] = rgb[1];
        this.cell.data[i_ofs + 2] = rgb[2];
        this.cell.data[i_ofs + 3] = 0xff;
        i_ofs += 4;

        bit >>= 1;
      }

      if(this.bits > 8) {
        var rgb = (c > 0x7f && this.maps[r_ofs] & 0x01) ? this.rgbf : this.rgbb;

        this.cell.data[i_ofs + 0] = rgb[0];
        this.cell.data[i_ofs + 1] = rgb[1];
        this.cell.data[i_ofs + 2] = rgb[2];
        this.cell.data[i_ofs + 3] = 0xff;
        i_ofs += 4;
      }

      r_ofs++;
    }
  }

  function nozzle_draw() {
    if(typeof(this.cell) === 'undefined') {
      this.scan();
    }
    this.head = this.ctx.createImageData(this.cell);
    this.head.data.set(this.cell.data);

    if(this.show) {
      for(var row = this.rmin; row <= this.rmax; row++) {
          var ofs = this.bits * row * 4;
          for(var col = 0; col < this.bits; col++) {
            this.head.data[ofs + 0] = this.rgbf[0];
            this.head.data[ofs + 1] = this.rgbf[1];
            this.head.data[ofs + 2] = this.rgbf[2];
            this.head.data[ofs + 3] = 0xff;
            ofs += 4;
        }
      }
    }

    this.ctx.putImageData(this.head, this.xpos * this.bits, this.ypos * this.rows);
  }

  function nozzle_move(xpos, ypos) {
    if(this.show) {
      this.show = false;
      this.draw();
      this.show = true;
    }

    this.xpos = xpos;
    this.ypos = ypos;

    this.scan();
    this.draw();
  }
})({ });
